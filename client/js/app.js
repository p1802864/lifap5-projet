/* global M getUser getQuizzes state filterHttpResponse getMyQuizzes getAnswers installWebSocket getMyQuizz getQuizz getAnswer */
// ajouter installWebSocket avant

// un simple ping/pong, pour montrer comment on envoie des données JSON au serveur
// eslint-disable-next-line no-unused-vars
const postEcho = (data) => {
	const url = `${state.serverUrl}/echo`;
	const body = JSON.stringify(data);
	return fetch(url, { method: 'POST', headers: state.headers, body })
		.then(filterHttpResponse)
		.catch(console.error);
};

// //////////////////////////////////////////////////////////////////////////////
// PROGRAMME PRINCIPAL
// //////////////////////////////////////////////////////////////////////////////

function app() {
	console.debug(`@app()`);
	// ici, on lance en parallèle plusieurs actions
	return Promise.all([getUser(),getQuizzes(),getMyQuizzes(),getAnswers()]).then(console.debug(`@app(): OK`));
		// .catch(console.debug('KO'));
}

// pour initialiser la bibliothèque Materialize
// https://materializecss.com/auto-init.html
M.AutoInit();

// lancement de l'application
app();

// permet de get les différents quizz sélectionné par l'utilisateur dans les différents onglets
function getNotUndefined(){
	if(state.currentQuizzId!==undefined){
		getQuizz(state.currentQuizzId);
	}
	if(state.currentMyQuizzId!==undefined){
		getMyQuizz(state.currentMyQuizzId);
	}
	if(state.answerNb!==undefined){
		getAnswer(state.answerNb);
	}
}

// raffraichi l'affichage de la page quand un changement est fait sur le serveur
const raffraichissement = ()=>Promise.all([getQuizzes(state.currentPage),getMyQuizzes(),getAnswers()]).then(getNotUndefined()).then(console.debug(`@raffraichissement(): OK`))

installWebSocket(raffraichissement);
