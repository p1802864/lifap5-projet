/* global state getQuizzes getQuizz getMyQuizzes getMyQuizz getAnswer getAnswers postQuizz put postQuestion repondre getUser supprimer */

// //////////////////////////////////////////////////////////////////////////////
// HTML : fonctions génération de HTML à partir des données passées en paramètre
// //////////////////////////////////////////////////////////////////////////////

// génération d'une liste de quizzes avec deux boutons en bas
const htmlQuizzesList = (quizzes, curr, total) => {
	console.debug(`@htmlQuizzesList(.., ${curr}, ${total})`);

	// un élement <li></li> pour chaque quizz. Noter qu'on fixe une donnée
	// data-quizzid qui sera accessible en JS via element.dataset.quizzid.
	// On définit aussi .modal-trigger et data-target="id-modal-quizz-menu"
	// pour qu'une fenêtre modale soit affichée quand on clique dessus
	// VOIR https://materializecss.com/modals.html
	const quizzesLIst = quizzes.map(
		(q) =>
			`<li id="Quizz-${q.quiz_id}" class="collection-item moddal-trigger cyan ${(q.quiz_id===state.currentQuizzId? "" : "lighten-5")}" style="cursor: pointer;" data-target="id-modal-quizz-menu" data-quizzid="${q.quiz_id}">
				<h5>${q.title}</h5>
				${q.description} <a class="chip">${q.owner_id}</a>
			</li>`
	);

	// le bouton "<" pour revenir à la page précédente, ou rien si c'est la première page
	// on fixe une donnée data-page pour savoir où aller via JS via element.dataset.page
	const prevBtn =
		curr !== 1
			? `<button id="id-prev-quizzes" data-page="${curr - 1}" class="btn"><i class="material-icons">navigate_before</i></button>`
			: '';

	// le bouton ">" pour aller à la page suivante, ou rien si c'est la première page
	const nextBtn =
		curr !== total
			? `<button id="id-next-quizzes" data-page="${curr + 1}" class="btn"><i class="material-icons">navigate_next</i></button>`
			: '';

	// La liste complète et les deux boutons en bas
	const html = `
	<ul class="collection">
		${quizzesLIst.join('')}
	</ul>
	<div class="row">      
		<div class="col s6 left-align">${prevBtn}</div>
		<div class="col s6 right-align">${nextBtn}</div>
	</div>
	`;
	return html;
};

const QuizzToHTML = (questions,reponse) => {
	let bouton
	if(state.user){
		if(state.currentQuizz.open){// si le quizz est open on n'indique pas que le quizz est closed
			bouton = '';
		}
		else{
			bouton = '<span style= "border:1px solid black; border-radius:5px;" >quizz closed</span>';
		}
	}
	else{// si l'utilisateur n'est pas connecter on lui demande de ce connecter
		bouton = '<span style= "border:1px solid black; border-radius:5px;" >connectez vous pour répondre !</span>';
	}
	return `
		<form id="form-quiz" action="#" method="get" class="reponse">${questions.map(n=>`<span style="font-size: 2.5em;">${n.question_id+1} : ${n.sentence}</span><br/>
			<div class="proposition${n.question_id}">
				${n.propositions.map(p=>`<label>

					<input ${
						// eslint-disable-next-line no-nested-ternary
						(reponse!==undefined
							? (reponse.find(r=>r.proposition_id===p.proposition_id&&r.question_id===n.question_id)!==undefined
								? 'checked="checked"'
								: '') 

							: '' )
					} type="radio" id="${p.proposition_id}" name="${n.question_id}" value="${p.proposition_id}"/><span style="color: dimgrey;">${p.content}</span>

				</label>`).join('<br/>\n')}
			</div>`).join('<br/>\n')}
			${bouton}
		</form>`;
};

// //////////////////////////////////////////////////////////////////////////////
// RENDUS : mise en place du HTML dans le DOM et association des événemets
// //////////////////////////////////////////////////////////////////////////////

// met la liste HTML dans le DOM et associe les handlers aux événements
// eslint-disable-next-line no-unused-vars
function renderQuizzes() {
	console.debug(`@renderQuizzes()`);

	// les éléments à mettre à jour : le conteneur pour la liste des quizz
	const usersElt = document.getElementById('id-all-quizzes-list');
	// une fenêtre modale définie dans le HTML

	// on appelle la fonction de généraion et on met le HTML produit dans le DOM
	usersElt.innerHTML = htmlQuizzesList(
		state.quizzes.results,
		state.quizzes.currentPage,
		state.quizzes.nbPages
	);

	// /!\ il faut que l'affectation usersElt.innerHTML = ... ait eu lieu pour
	// /!\ que prevBtn, nextBtn et quizzes en soient pas null
	// les éléments à mettre à jour : les boutons
	const prevBtn = document.getElementById('id-prev-quizzes');
	const nextBtn = document.getElementById('id-next-quizzes');
	// la liste de tous les quizzes individuels

	// les handlers quand on clique sur "<" ou ">"
	function clickBtnPager() {
		// remet à jour les données de state en demandant la page
		// identifiée dans l'attribut data-page
		// noter ici le 'this' QUI FAIT AUTOMATIQUEMENT REFERENCE
		// A L'ELEMENT AUQUEL ON ATTACHE CE HANDLER
		getQuizzes(this.dataset.page);
	}
	if (prevBtn) prevBtn.onclick = clickBtnPager;
	if (nextBtn) nextBtn.onclick = clickBtnPager;

	// qd on clique sur un quizz, on change sont contenu avant affichage
	// l'affichage sera automatiquement déclenché par materializecss car on
	// a définit .modal-trigger et data-target="id-modal-quizz-menu" dans le HTML
	function clickQuiz(id) {
		const main = document.getElementById('id-all-quizzes-main');
		main.innerHTML=`<img src="https://thumbs.gfycat.com/KnobbyWeirdIlladopsis.webp"/>`;
		state.currentQuizzId=id;
		getQuizz(id);
	}
	// pour chaque quizz, on lui associe son handler
	const Quizzes = state.quizzes.results;
	Quizzes.map(p=>{document.getElementById(`Quizz-${p.quiz_id}`).onclick = () => clickQuiz(p.quiz_id);return 0;});
}

function HTMLquizzList(Quizzes,ID){
	let qId = -1;
	if(ID==="quizz"){
		qId = state.currentMyQuizzId;
	}
	else if(ID === "answer"){
		qId = state.currentAnswerId;
	}
	return `
	<ul class="collection" id="list_${ID}">
			${Quizzes.reduce((acc,q)=>
					`${acc}
					<li id="${ID}_${q.quiz_id}" class="collection-item moddal-trigger cyan ${(q.quiz_id===qId? "" : "lighten-5")}" style="cursor: pointer;" >
						<h5>${q.title}</h5>
						${q.description} <a class="chip">${q.owner_id}</a>
					</li>`	,"")
				}
	</ul>`;

}

function renderCreate(){
	const bouton = document.getElementById('create').classList;
	bouton.remove("cyan");
	bouton.add("blue");
	const main = document.getElementById("id-my-quizzes-main");

	main.innerHTML=`
	<form id="form_create" method="get">
		<input type="text" placeholder="titre" name="titre" />
		<textarea placeholder="description..." name="description"></textarea>
		<button class="btn waves-effect waves-light right" type="submit">créer !</button>
	</form>`;

	const form = document.getElementById('form_create');
	form.onsubmit = function créerUnQuizz(ev){ 
		ev.preventDefault();
		const formData = new FormData(this);
		const tab = [];
		// eslint-disable-next-line no-restricted-syntax
		for(const pair of formData.entries()){
			tab.push(pair);
		}

		postQuizz(tab[0][1],tab[1][1])
			.then(getQuizzes())
			.then(getMyQuizzes())
			.catch(console.debug("erreur quizz non posté"));


		// eslint-disable-next-line no-alert
		alert("vous avez créé un quizz !");
	}
}

/* eslint-disable-next-line no-unused-vars */
function renderMyQuizzes(Quizzes=state.MyQuizzes){

	const myquizz = document.getElementById('id-my-quizzes-list');
	myquizz.innerHTML=HTMLquizzList(Quizzes,"quizz");
	const list = document.getElementById("list_quizz");
	list.innerHTML+=`<li id="create" class="collection-item moddal-trigger cyan" style="font-size:5em;text-align:center;cursor:pointer;color:white;">+</li>`;

	Quizzes.map(p=>{
		document.getElementById(`quizz_${p.quiz_id}`).onclick=()=>{
				const main = document.getElementById('id-my-quizzes-main');
				main.innerHTML=`<img src="https://thumbs.gfycat.com/KnobbyWeirdIlladopsis.webp"/>`;
				state.currentMyQuizzId=p.quiz_id;
				getMyQuizz(p.quiz_id);
			};
			return 0;
		}
	);

	document.getElementById("create").onclick=()=>{state.currentMyQuizzId=undefined;renderMyQuizzes();renderCreate();}
}

/* eslint-disable-next-line no-unused-vars */
function renderAnswers(Quizzes=state.Answers){

	const answers = document.getElementById('id-answers-list');
	answers.innerHTML= HTMLquizzList(Quizzes,"answer");
	Quizzes.map((p,n)=>{
		document.getElementById(`answer_${p.quiz_id}`).onclick=()=>{
			const main = document.getElementById('id-answer-main');
			main.innerHTML=`<img src="https://thumbs.gfycat.com/KnobbyWeirdIlladopsis.webp"/>`;
			state.currentAnswerId=p.quiz_id;
			state.answerNb = n;
			getAnswer(n);
		};
		return 0;
	});
}

/* eslint-disable-next-line no-unused-vars */
function renderAnswer(questions,answers){
	const main = document.getElementById('id-answer-main');
	
	// affichage du quizz à la manière des autres affichages sauf qu'il n'y a pas de formulaire
	main.innerHTML = questions.reduce((acc,n,i)=>`${acc}
		<span style="font-size:2.5em;">${i+1} : ${n.sentence}</span><br/>
		<div class="proposition${n.question_id}">
			${n.propositions.reduce((acc2,p)=>`${acc2}<span style="margin-left:1em;">${p.proposition_id+1} : ${p.content}</span><br/>\n`,"")}
		</div><br/>
		<div id="reponse${n.question_id}" style="font-size: 1.2em;">
			vous n'avez pas répondu a cette question !
		</div>`,"");// par défaut on met "vous n'avez pas répondu a cette question"

	// ensuite on parcour les réponse pour changer le texte "vous n'avez pas répondu a cette question" si l'utilisateur a répondu
	answers.map(rep=>{
		const proposition = document.getElementById(`reponse${rep.question_id}`);
		proposition.innerHTML=`vous avez répondu : ${questions[rep.question_id].propositions[rep.proposition_id].content}`;
		return 0;
	});
}


const MyQuizzToHTML = (questions) => {
	// création d'un formulaire pour voir les questions de "my quizz" sélectionné 
	return `
		<form id="form-quiz" action="#" method="get" class="reponse">
			${questions.map(n=>`
				<span style="font-size: 2.5em;">${n.question_id+1} : ${n.sentence}</span><br/>
				<div class="proposition${n.question_id}">
				${n.propositions.map(p=>`
					<label>
						<input type="radio" id="${p.proposition_id}" name="${n.question_id}" value="${p.proposition_id}"/><span style="color: dimgrey;">${p.content}</span>
					</label>`)
				.join('<br/>\n')}
				</div>
				<button id="del-${n.question_id}" type="button" class="btn waves-effect waves-light red">
					supprimer <i class="material-icons right">clear</i>
				</button>
				<br/><br/>`)
			.join('<br/>\n')}
			${(state.currentQuizz.open // si le quizz est fermé on met un bouton pour l'ouvrir et inversement
				? '<button id="close_quizz" class="btn waves-effect waves-light" type="button">fermer le quizz</button>' 
				: '<button id="open_quizz" class="btn waves-effect waves-light" type="button">ouvrire le quizz</button >'
			)}
		</form>`;
};

function createQuestion(questionId,quizzId){
	// création du formulaire d'ajout de question
	document.getElementById('id-create-main').innerHTML=
		`<br/><br/>
		<form id="ajout_question" method="get">
			<input type="text" name="intitule" placeholder="intitulée de la question" style="font-size:2.5em;"/>
			<div id="propositions">
				<input type="text" name="1"/><label>proposition 1</label>
				<input type="text" name="2"/><label>proposition 2</label>
			</div>
			<select name="correct_answer" size="1" id="correct" style="display:inline;">
				<option value="1">1</option>
				<option value="2">2</option>
			</select>
			<button type="button" id="add_proposition">ajouter une proposition</button>
			<button type="submit">ajouter !</button>
		</form>`;

	// action du bouton pour ajouter une proposition
	document.getElementById('add_proposition').onclick=()=>{
		const props = document.getElementById('propositions');
		const nbProps = (props.childNodes.length-1)/2;
		const input = document.createElement("INPUT");
		input.type="text"; input.name = nbProps;
		const label = document.createElement("LABEL");

		// on utilise appendChild() pour ne pas effacer ce qu'il y avait dans les anciennes propositions
		label.appendChild(document.createTextNode(`proposition ${nbProps}`));
		props.appendChild(input);
		props.appendChild(label);
		const options = document.getElementById('correct');
		options.innerHTML+=`<option value="${nbProps}">${nbProps}</option>`
	};

	// quand le formulaire d'ajout d'une question est valider
	document.getElementById('ajout_question').onsubmit = 
		function addQuestion(ev){
			ev.preventDefault();
			const formData = new FormData(this);
			const propositions = [];
			const data = {};
			// eslint-disable-next-line no-restricted-syntax
			for(const pair of formData.entries()){
				if(pair[0]!=="intitule"&&pair[0]!=="correct_answer"){
					propositions.push(pair[1]);
				}
				else{
					// eslint-disable-next-line prefer-destructuring
					data[pair[0]]=pair[1];
				}
			}
			// création du body pour poster une question
			const object = `
				{	"question_id" : ${questionId},
					"sentence" : "${data.intitule}",
					"propositions" :
						[${propositions.map((n,i)=>`
							{	"content" : "${n}", 
								"proposition_id" : ${i}, 
								"correct" : ${(i===data.correct_answer-1? "true":"false")} 
							}`).join(",")}
						]
				}`;

			console.debug(object);
			postQuestion(quizzId,object)
				.then(()=>{
					const main = document.getElementById('id-my-quizzes-main');
					// image de chargement dans la page principal de mes quizzes
					main.innerHTML=`<img src="https://thumbs.gfycat.com/KnobbyWeirdIlladopsis.webp"/>`;
					state.currentMyQuizzId=quizzId;
					return getMyQuizz(quizzId);// on "re-get" my quizz pour l'avoir avec la nouvelle question
				})
				.catch(console.debug("question non postée"));
		}
}

/* eslint-disable-next-line no-unused-vars */
function renderMyQuizz(questions){
	const id = state.currentMyQuizzId;
	const qi = questions.reduce((acc,n)=> n.question_id ,"");
	const questionId = (qi===""? 0:qi+1);// questionId contient l'id d'une question rajoutée

	const bouton = document.getElementById('create').classList;
	// on remet la couleur du bouton pour ajouter un quizz quand on sélectionne un quizz (car on quitte l'interface de création de quizz)
	bouton.remove("blue");
	bouton.add("cyan");

	const main = document.getElementById('id-my-quizzes-main');
	main.innerHTML = `${MyQuizzToHTML(questions)}<br/><div id="id-create-main"><button id="add_question">ajouter une questions !</button></div>`;

	// pour chaques boutons supprimer on aplique la fonction qui supprime la fonction sur le serveur
	questions.map(q=>{
		document.getElementById(`del-${q.question_id}`).onclick = () => supprimer(q.quiz_id, q.question_id);
		return 0;
	});

	if(state.currentQuizz.open){// si le quizz est ouvert on détecte l'appuie sur le bouton 'close_quizz' pour le fermer
		document.getElementById('close_quizz').onclick=()=>{
			// on met une image de chargement dans le bouton
			document.getElementById('close_quizz').innerHTML=`<img class="in-bouton" src="https://www.jogging-plus.com/wp-content/uploads/loading-large.gif"/>`;
			put('false',id);
		};
	}
	else{
		document.getElementById('open_quizz').onclick=()=>{// si le quizz est fermer on détecte l'appuie sur le bouton 'open_quizz'
			document.getElementById('open_quizz').innerHTML=`<img class="in-bouton" src="https://www.jogging-plus.com/wp-content/uploads/loading-large.gif"/>`;
			put('true',id);
		};
	}

	document.getElementById('add_question').onclick=()=>createQuestion(questionId,id);
}

/* eslint-disable-next-line no-unused-vars */
function renderCurrentQuizz(questions,reponse) {// met le quizz et les questions dans le main
	const Open = state.currentQuizz.open;
	const main = document.getElementById('id-all-quizzes-main');
	main.innerHTML = QuizzToHTML(questions,reponse);
	const id = state.currentQuizzId;
	const form = document.getElementById('form-quiz');

	// pas optimal car ici on met a jour toutes les questions à chaques changement alors qu'on change qu'une seul question,
	// pour bien faire il faudrait un formulaire par question 
	form.onchange = function répondre(ev) {
		if(state.user&&Open){
			ev.preventDefault();
			const formData = new FormData(this);
			// eslint-disable-next-line no-restricted-syntax
			for(const pair of formData.entries()){
				repondre(id,pair[0],pair[1])
					.catch(console.debug("réponse impossible"));
			}
		}
	}
}

// quand on clique sur le bouton de login, il nous dit qui on est
// eslint-disable-next-line no-unused-vars
const renderUserBtn = () => {
	const btn = document.getElementById('id-login');
	btn.style="cursor: pointer;"
	btn.onclick = () => {
		if (state.user) {
			// eslint-disable-next-line no-restricted-globals,no-alert
			const saisie=confirm(
				`vous êtes connecté en tant que ${state.user.firstname} ${state.user.lastname}\nVous voulez vous déconnecter ? :`// on peut pas se déloguer mais seulement changer d'utilisateur.
			);
			if(saisie){
				state.xApiKey='';
			}
		} else {
			// eslint-disable-next-line no-alert
			const saisie=prompt(
				`Pour vous authentifier, rentrez votre clef`
			);
			if(saisie!==null&&saisie!==''){
				state.xApiKey=saisie;
			}
		}
		getUser()
			.then(getMyQuizzes())
			.then(getAnswers())
			.then(()=>{
					const main=document.getElementById('id-all-quizzes-main');
					main.innerHTML = "";
					state.currentQuizzId = undefined;
					return renderQuizzes();
				})
			.catch(console.debug("erreur lors du chargement de la page"));
	};
};
