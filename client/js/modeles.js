/* globals renderQuizzes renderUserBtn renderMyQuizz renderMyQuizzes renderAnswers renderAnswer renderCurrentQuizz */

// //////////////////////////////////////////////////////////////////////////////
// LE MODELE, a.k.a, ETAT GLOBAL
// //////////////////////////////////////////////////////////////////////////////

// un objet global pour encapsuler l'état de l'application
// on pourrait le stocker dans le LocalStorage par exemple
const state = {
	// la clef de l'utilisateur je met la mienne de base pour les test mais la page gère bien les utilisateurs déconnecté
	xApiKey: 'bb26e70b-5165-41c9-b8d1-4ad1d5ecb03f',

	// l'URL du serveur où accéder aux données
	serverUrl: 'https://lifap5.univ-lyon1.fr',

	// la liste des quizzes
	quizzes: [],
	currentPage : 1,

	// la liste de tes quizzes
	MyQuizzes: [],

	// la liste des quizzes aux - quels tu as répondu.
	Answers: [],

	// le quizz, mon quizz et mes réponse actuellement choisi(s)
	currentQuizzId: undefined,
	currentMyQuizzId: undefined,
	currentAnswerId: undefined,
	answerNb: undefined,// numero de la réponse (différente de l'id du quizz répondu)

	currentQuizz: undefined,

	headers(){
		const headers = new Headers();
		headers.set('X-API-KEY', this.xApiKey);
		headers.set('Accept', 'application/json');
		headers.set('Content-Type', 'application/json');
		return headers;
	}
};

// une méthode pour l'objet 'state' qui va générer les headers pour les appel à fetch

// //////////////////////////////////////////////////////////////////////////////
// OUTILS génériques
// //////////////////////////////////////////////////////////////////////////////

// un filtre simple pour récupérer les réponses HTTP qui correspondent à des
// erreurs client (4xx) ou serveur (5xx)
/* eslint-disable-next-line no-unused-vars */
function filterHttpResponse(response) {
	return response
		.json()
		.then((data) => {
			if (response.status >= 400 && response.status < 600) {
				throw new Error(`${data.name}: ${data.message}`);
			}
			return data;
		})
		.catch((err) => console.error(`Error on json: ${err}`));
}

// charge une donnees a l'url url, puis appel le callback (renvoie juste l'objet json si aucun callback n'es donné)
const charge = (url,fonction = (data)=>data) => {
	return fetch(url, {method: 'GET',headers: state.headers()})
		.then(filterHttpResponse)
		.then(fonction);
}

/* eslint-disable-next-line no-unused-vars */
const repondre = (id,questionId,propositionId) =>{

	const url = `quizzes/${id}/questions/${questionId}/answers/${propositionId}`;
	return fetch(`${state.serverUrl}/${url}`, {method: 'POST',headers: state.headers()})
		.then(filterHttpResponse);
}

/* eslint-disable-next-line no-unused-vars */
const postQuizz = (title,description)=>
	fetch(`${state.serverUrl}/quizzes/`, 
		{method: 'POST',
		body:`{"title":"${title}","description":"${description}"}`,
		headers: state.headers()})
		.then(filterHttpResponse);

/* eslint-disable-next-line no-unused-vars */
const postQuestion = (id,question)=>
	fetch(`${state.serverUrl}/quizzes/${id}/questions`,
		{method: 'POST',
		body: question,
		headers: state.headers()})
		.then(filterHttpResponse);

/* eslint-disable-next-line no-unused-vars */
const put = (etat,id)=>
{
	const url = `${state.serverUrl}/quizzes/${id}`;
	return charge(url,data=>
		fetch(url,
			{method: 'PUT',
			body: `{"title":"${data.title}","description":"${data.description}","open":${etat}}`,
			headers: state.headers()})
		.then(filterHttpResponse));
}

/* eslint-disable-next-line no-unused-vars */
const supprimer = (quizzId,questionId) =>
	fetch(`${state.serverUrl}/quizzes/${quizzId}/questions/${questionId}`,
		{method: 'DELETE',
		headers: state.headers()})
	.then(filterHttpResponse);

// //////////////////////////////////////////////////////////////////////////////
// DONNEES DES UTILISATEURS
// //////////////////////////////////////////////////////////////////////////////

// mise-à-jour asynchrone de l'état avec les informations de l'utilisateur
// l'utilisateur est identifié via sa clef X-API-KEY lue dans l'état
// eslint-disable-next-line no-unused-vars
const getUser = () => {
		console.debug(`@getUser()`);
		const url = `${state.serverUrl}/users/whoami`;
		return charge(url,(data) => {
			// /!\ ICI L'ETAT EST MODIFIE /!\
			state.user = data;
			// on lance le rendu du bouton de login
			renderUserBtn();
		});
};

// //////////////////////////////////////////////////////////////////////////////
// DONNEES DES QUIZZES
// //////////////////////////////////////////////////////////////////////////////

// mise-à-jour asynchrone de l'état avec les informations de l'utilisateur
// getQuizzes télécharge la page 'p' des quizzes et la met dans l'état
// puis relance le rendu
// eslint-disable-next-line no-unused-vars
const getMyQuizzes = () => {
	const title = document.getElementById('id-my-quizzes-title');
	console.debug(`@getMyQuizzes()`);
	if(state.xApiKey===''){
		const list = document.getElementById('id-my-quizzes-list');
		const main = document.getElementById('id-my-quizzes-main');
		list.innerHTML="";
		main.innerHTML="";
		title.innerHTML=`connectez vous pour acceder à ce contenue`;
	}
	else{
		title.innerHTML="";
		const url = `${state.serverUrl}/users/quizzes`;
		charge(url,(data)=>{
			state.MyQuizzes = data;
			renderMyQuizzes();
		})
	}
}

/* eslint-disable-next-line no-unused-vars */
const getMyQuizz = (id) => {
	console.debug(`@getMyQuizz(${id})`);
	const url = `${state.serverUrl}/quizzes/${id}`;
	charge(url,quizz=>{
		state.currentQuizz=quizz;
 		return charge(`${url}/questions`,(questions)=>{renderMyQuizz(questions);renderMyQuizzes();});
	})
}

/* eslint-disable-next-line no-unused-vars */
const getAnswers = () => {
	const title = document.getElementById('id-my-answers-title');
	console.debug(`@getAnswers()`);
	if(state.xApiKey!==''){
		title.innerHTML="";
		const url = `${state.serverUrl}/users/answers`;
		charge(url,data=>{
			state.Answers = data;
			renderAnswers();
		});
	}
	else{
		const list = document.getElementById('id-answers-list');
		const main = document.getElementById('id-answer-main');
		list.innerHTML="";
		main.innerHTML="";
		title.innerHTML=`connectez vous pour acceder à ce contenue`;
	}
}

/* eslint-disable-next-line no-unused-vars */
const getAnswer = (id) => {
	console.debug(`@getAnswer(${id})`);
	const url = `${state.serverUrl}/quizzes/${state.Answers[id].quiz_id}`;
	charge(url,data=>{
		state.currentQuizz=data;
		return charge(`${url}/questions`,questions=>{renderAnswer(questions,state.Answers[id].answers);renderAnswers();})
	});
}

/* eslint-disable-next-line no-unused-vars */
const getQuizzes = (p = state.currentPage) => {
	state.currentPage = p;
	console.debug(`@getQuizzes(${p})`);
	const url = `${state.serverUrl}/quizzes/?page=${p}`;
	// le téléchargement est asynchrone, là màj de l'état et le rendu se fait dans le '.then'
	return charge(url,(data)=>{state.quizzes = data;renderQuizzes();});
};

/* eslint-disable-next-line no-unused-vars */
const getQuizz = (id) => {// met le quizz d'id id dans l'etat actuel
	const url = `${state.serverUrl}/quizzes/${id}`;
	charge(url,(data)=>{
		state.currentQuizz=data;
		return charge(`${url}/questions`,questions=>{
			if(state.xApiKey!==''){// si l'utilisateur est connecter on cherche si le quizz a déjà été répondu
				charge(`${state.serverUrl}/users/answers`,reponses=>{
					const exist = reponses.find(r=>r.quiz_id===id)
					if(exist !== undefined){// si le quizz courant a déjà été répondu on affiche le quiz avec les réponses
						renderQuizzes();
						renderCurrentQuizz(questions,exist.answers);
					}
					else{
						renderQuizzes();
						renderCurrentQuizz(questions);
					}
				});
			}
			else{
				renderQuizzes();
				renderCurrentQuizz(questions);
			}
		});
	});
}
